<?php
/**
 * Magento
 *
 * @category    Kaliop
 * @package     Kaliop_StoreLocator
 * @copyright   Copyright (c) 2019 Kaliop Poland Sp. z o.o. (https://www.kaliop.pl/)
 * @license     ? http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author      Filip Wachowiak <filip0606@o2.pl>
 */
class Kaliop_StoreLocator_Model_Carrier extends Mage_Shipping_Model_Carrier_Abstract implements Mage_Shipping_Model_Carrier_Interface 
{
    protected $_code = 'storelocator_shipping';
    /**
     * Points Collection
     * 
     * @var null|Kaliop_StoreLocator_Model_Resource_Storelocator_Collection
     */
    protected $_collection = null;
    
    public function getAllowedMethods()
    {
        $allowedMethods = [];
        
        foreach ($this->_getCollection() as $storePoint) {
            $key = "storelocator-{$storePoint->getId()}";
            $allowedMethods[$key] = $storePoint->getName();
        }
        
        return $allowedMethods;
    }
    
    public function collectRates(Mage_Shipping_Model_Rate_Request $request) 
    {
	$result = Mage::getModel('shipping/rate_result');
        
        $storePointsCollection = $this->_getCollection();
        foreach ($storePointsCollection as $storePoint) {
            $rate = Mage::getModel('shipping/rate_result_method');
            $rate->setCarrier($this->_code);
            $rate->setCarrierTitle($this->getConfigData('title'));
            $rate->setMethod("storelocator-{$storePoint->getId()}");
            $rate->setMethodTitle($storePoint->getName());
            $rate->setPrice(0);
            $rate->setCost(0);
            
            $result->append($rate);
        }
 
	return $result;
    }
    
    /**
     * Returns Points Collection
     * 
     * @return Kaliop_StoreLocator_Model_Resource_Storelocator_Collection
     */
    protected function _getCollection()
    {
        if (is_null($this->_collection)) {
            $this->_collection = Mage::getModel('storelocator/storelocator')->getCollection();
        }
        
        return $this->_collection;
    }
}

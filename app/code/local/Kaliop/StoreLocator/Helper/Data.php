<?php
/**
 * Magento
 *
 * @category    Kaliop
 * @package     Kaliop_StoreLocator
 * @copyright   Copyright (c) 2019 Kaliop Poland Sp. z o.o. (https://www.kaliop.pl/)
 * @license     ? http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author      Filip Wachowiak <filip0606@o2.pl>
 */
class Kaliop_StoreLocator_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Storelocator Config Path
     */
    const XML_PATH_ACTIVE = 'carriers/storelocator_shipping/active';
    
    /**
     * Returns true If Storelocator Shipping Method Is Enabled, false Otherwise
     * 
     * @param int $storeId
     * @return boolean
     */
    public function isActive($storeId = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_ACTIVE, $storeId);
    }
    
    /**
     * Returns Points Images Directory
     * 
     * @return string
     */
    public function getFilesDirectory()
    {
        return Mage::getBaseDir('media') . DS . 'kaliop_storelocator' . DS;
    }
    
    /**
     * Returns Points Images Url
     * 
     * @return string
     */
    public function getFilesUrl()
    {
        return Mage::getBaseUrl('media') . 'kaliop_storelocator/';
    }
    
    /**
     * Returns Point Image Url
     * 
     * @param string $fileName
     * @return string
     */
    public function getFileUrl($fileName)
    {
        return $this->getFilesUrl() . $fileName . '/' . $fileName;
    }
}

<?php
/**
 * Magento
 *
 * @category    Kaliop
 * @package     Kaliop_StoreLocator
 * @copyright   Copyright (c) 2019 Kaliop Poland Sp. z o.o. (https://www.kaliop.pl/)
 * @license     ? http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author      Filip Wachowiak <filip0606@o2.pl>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
 
$table = $installer->getConnection()
    ->newTable($installer->getTable('storelocator/storelocator_point'))
    ->addColumn('point_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Id')
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable'  => false,
        ), 'Name')
    ->addColumn('city', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable'  => false,
        ), 'City')
    ->addColumn('street', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable'  => false,
        ), 'Street')
    ->addColumn('file_name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable'  => true,
        ), 'File Name');
$installer->getConnection()->createTable($table);
  
$installer->endSetup();

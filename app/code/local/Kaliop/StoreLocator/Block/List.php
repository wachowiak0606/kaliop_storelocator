<?php
/**
 * Magento
 *
 * @category    Kaliop
 * @package     Kaliop_StoreLocator
 * @copyright   Copyright (c) 2019 Kaliop Poland Sp. z o.o. (https://www.kaliop.pl/)
 * @license     ? http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author      Filip Wachowiak <filip0606@o2.pl>
 */
class Kaliop_StoreLocator_Block_List extends Mage_Core_Block_Template
{
    /**
     * Returns STorelocator Points
     * 
     * @return Kaliop_StoreLocator_Model_Resource_Storelocator_Collection
     */
    public function getPoints()
    {
        return Mage::getModel('storelocator/storelocator')
            ->getCollection()
            ->setOrder('point_id', 'DESC')
        ;
    }
    
    /**
     * Returns url to point details
     * 
     * @param int $pointId
     * @return string
     */
    public function getDetailsUrl($pointId)
    {
        return Mage::getUrl('storelocator/list/details', ['point_id' => $pointId]);
    }
    
    protected function _prepareLayout()
    {
        $this->getLayout()->getBlock('head')->setTitle(Mage::helper('storelocator')
            ->__('Store Points List'));
        
        return parent::_prepareLayout();
    }
}

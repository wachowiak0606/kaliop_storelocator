<?php
/**
 * Magento
 *
 * @category    Kaliop
 * @package     Kaliop_StoreLocator
 * @copyright   Copyright (c) 2019 Kaliop Poland Sp. z o.o. (https://www.kaliop.pl/)
 * @license     ? http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author      Filip Wachowiak <filip0606@o2.pl>
 */
class Kaliop_StoreLocator_Block_Adminhtml_Storelocator_Grid_Renderer_FileName 
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row) { 
        $value = $row->getData($this->getColumn()->getIndex());
        $hasImage = $this->__('No');
        
        if (!empty($value)) {
            $hasImage = $this->__('Yes');
        }
        
        return '<span style="color: red;">'.$hasImage.'</span>';
    }
}

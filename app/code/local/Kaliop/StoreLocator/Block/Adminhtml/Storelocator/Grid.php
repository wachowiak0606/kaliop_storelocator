<?php
/**
 * Magento
 *
 * @category    Kaliop
 * @package     Kaliop_StoreLocator
 * @copyright   Copyright (c) 2019 Kaliop Poland Sp. z o.o. (https://www.kaliop.pl/)
 * @license     ? http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author      Filip Wachowiak <filip0606@o2.pl>
 */
class Kaliop_StoreLocator_Block_Adminhtml_Storelocator_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
         
        $this->setDefaultSort('point_id');
        $this->setId('storelocator_storelocator_grid');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);
    }
    
    /**
     * Returns Points Resource Model Collection Identifier
     * 
     * @return string
     */
    protected function _getCollectionClass()
    {
        return 'storelocator/storelocator_collection';
    }
    
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel($this->_getCollectionClass());
        $this->setCollection($collection);
         
        return parent::_prepareCollection();
    }
    
    protected function _prepareColumns()
    {
        $this->addColumn('point_id',
            array(
                'header'=> $this->__('ID'),
                'align' =>'right',
                'width' => '50px',
                'index' => 'point_id'
            )
        );
         
        $this->addColumn('name',
            array(
                'header'=> $this->__('Name'),
                'index' => 'name'
            )
        );
        
        $this->addColumn('city',
            array(
                'header'=> $this->__('City'),
                'index' => 'city'
            )
        );
        
        $this->addColumn('street',
            array(
                'header'=> $this->__('Street'),
                'index' => 'street'
            )
        );
        
        $this->addColumn('file_name',
            array(
                'header'=> $this->__('Has Image'),
                'index' => 'file_name',
                'renderer' => 'Kaliop_StoreLocator_Block_Adminhtml_Storelocator_Grid_Renderer_FileName',
                'filter_condition_callback' => array($this, '_fileNameFilter'),
            )
        );
        
        return parent::_prepareColumns();
    }
    
    /**
     * Filter Points Collection Depends On Image Exists Or Not
     * 
     * @param Kaliop_StoreLocator_Model_Resource_Storelocator_Collection $collection
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     * @return $this
     */
    protected function _fileNameFilter($collection, $column)
    {
        $value = $column->getFilter()->getValue();

        if ($value == 'Yes') {
            $collection->getSelect()->where('file_name IS NOT NULL');
        } else {
            $collection->getSelect()->where('file_name IS NULL');
        }
        
        return $this;
    }
    
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id'=>$row->getId()));
    }
}

<?php
/**
 * Magento
 *
 * @category    Kaliop
 * @package     Kaliop_StoreLocator
 * @copyright   Copyright (c) 2019 Kaliop Poland Sp. z o.o. (https://www.kaliop.pl/)
 * @license     ? http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author      Filip Wachowiak <filip0606@o2.pl>
 */
class Kaliop_StoreLocator_Block_Adminhtml_Storelocator_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
            'id'      => 'edit_form',
            'action'  => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
            'method'  => 'post',
            'enctype' => 'multipart/form-data'
        ));
        
        $fieldset = $form->addFieldset('storelocator_form',
            array('legend' => 'Store Locator Data')
        );
        
        $model = Mage::registry('storelocator_data');
        if ($model->getId()) {
            $fieldset->addField('point_id', 'hidden', array(
                'name' => 'point_id',
            ));
        }
        
        $fieldset->addField('name', 'text',
            array(
                'label'    => 'Name',
                'class'    => 'required-entry',
                'required' => true,
                'name'     => 'name',
            )
        );
        $fieldset->addField('city', 'text',
            array(
                'label'    => 'City',
                'class'    => 'required-entry',
                'required' => true,
                'name'     => 'city',
            )
        );
        $fieldset->addField('street', 'text',
            array(
                'label'    => 'Street',
                'class'    => 'required-entry',
                'required' => true,
                'name'     => 'street',
            )
        );
        $fieldset->addField('file_name', 'image',
            array(
                'label' => Mage::helper('storelocator')->__('Image'),
                'required' => false,
                'name' => 'file_name',
            )
        );

        if (Mage::registry('storelocator_data'))
        {
            $modelData = Mage::registry('storelocator_data')->getData();
            $fileName = $modelData['file_name'];
            if (!empty($fileName)) {
                $modelData['file_name'] = Mage::helper('storelocator')->getFileUrl($fileName);
            }
            
            $form->setValues($modelData);
        }
        $form->setUseContainer(true);
        $this->setForm($form);
    
        return parent::_prepareForm();
    }
}

<?php
/**
 * Magento
 *
 * @category    Kaliop
 * @package     Kaliop_StoreLocator
 * @copyright   Copyright (c) 2019 Kaliop Poland Sp. z o.o. (https://www.kaliop.pl/)
 * @license     ? http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author      Filip Wachowiak <filip0606@o2.pl>
 */
class Kaliop_StoreLocator_Block_Adminhtml_Storelocator_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'id';
        $this->_blockGroup = 'storelocator';
        $this->_controller = 'adminhtml_storelocator';
        $this->_updateButton('save', 'label', 'Save Store Locator');
        $this->_updateButton('delete', 'label', 'Delete Store Locator');
    }
    
    public function getHeaderText()
    {
        if (Mage::registry('storelocator_data') && Mage::registry('storelocator_data')->getId()) {
            return $this->__('Edit a store locator') . ' ' . $this->htmlEscape(Mage::registry('storelocator_data')->getName());
        } else {
            return $this->__('Add new store locator');
        }
    }
}

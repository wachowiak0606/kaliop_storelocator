<?php
/**
 * Magento
 *
 * @category    Kaliop
 * @package     Kaliop_StoreLocator
 * @copyright   Copyright (c) 2019 Kaliop Poland Sp. z o.o. (https://www.kaliop.pl/)
 * @license     ? http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author      Filip Wachowiak <filip0606@o2.pl>
 */
class Kaliop_StoreLocator_Block_List_Details extends Mage_Core_Block_Template
{
    /**
     * Returns Point IMage Url
     * 
     * @param string $fileName
     * @return string
     */
    public function getFileUrl($fileName)
    {
        return Mage::helper('storelocator')->getFileUrl($fileName);
    }
    
    /**
     * Returns Point Model
     * 
     * @return Kaliop_StoreLocator_Model_Storelocator
     */
    public function getPoint()
    {
        $pointId = $this->getRequest()->getParam('point_id');
        
        return Mage::getModel('storelocator/storelocator')->load($pointId);
    }
    
    /**
     * Returns Point List Url
     * 
     * @return string
     */
    public function getListUrl()
    {
        return Mage::getUrl('storelocator/list/index');
    }
    
    protected function _prepareLayout()
    {
        $this->getLayout()->getBlock('head')->setTitle(Mage::helper('storelocator')
            ->__('Store Point Details'));
        
        return parent::_prepareLayout();
    }
}

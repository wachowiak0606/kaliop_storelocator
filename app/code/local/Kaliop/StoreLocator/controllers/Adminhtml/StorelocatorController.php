<?php
/**
 * Magento
 *
 * @category    Kaliop
 * @package     Kaliop_StoreLocator
 * @copyright   Copyright (c) 2019 Kaliop Poland Sp. z o.o. (https://www.kaliop.pl/)
 * @license     ? http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author      Filip Wachowiak <filip0606@o2.pl>
 */
class Kaliop_StoreLocator_Adminhtml_StorelocatorController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Loads and Renders Layout,
     * Sets Active Menu Tab To Storelocator One,
     * Sets Head Title
     */
    public function indexAction()
    {   
        $this->loadLayout();
        $this->_setActiveMenu('storelocator/manage');
        $this->_title($this->__('Store Locators Grid'));
        $this->renderLayout();
    }
    
    /**
     * Forward To Edit Action
     */
    public function newAction()
    {
        $this->_forward('edit');
    }
    
    /**
     * Loads And Sets Editting Point Form
     * 
     * @return
     */
    public function editAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('storelocator/manage');
        
        $this->_title($this->__('Store Locators'));

        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('storelocator/storelocator');

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('storelocator')->__('This Store Point no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
        }

        $this->_title($model->getId() ? $model->getName() : $this->__('New Store Locator'));

        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }
        
        Mage::register('storelocator_data', $model);
        
        $this->renderLayout();
    }
    
    /**
     * Saves Point
     * 
     * @return
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $model = Mage::getModel('storelocator/storelocator');
            $id = $this->getRequest()->getParam('id');
            if ($id) {
                $model->load($id);
            }
            
            if (isset($data['file_name']['delete']) && $data['file_name']['delete'] == 1) {
                $fileName = '';
            } else {
                $fileName = $model->getFileName();
                // try to upload file if given
                $errorFileUploading = '';
                if (isset($_FILES['file_name']['name']) && $_FILES['file_name']['name'] != '') {
                    try {
                        $directory = Mage::helper('storelocator')->getFilesDirectory();
                        $fileName = $_FILES['file_name']['name'];
                        $filePath = $directory . $fileName;
                        $uploader = new Varien_File_Uploader('file_name');
                        $uploader->setAllowedExtensions(['JPG','jpg','PNG','png']);
                        $uploader->setAllowCreateFolders(true);
                        $uploader->setAllowRenameFiles(false);
                        $uploader->setFilesDispersion(false);
                        $uploader->save($filePath, $fileName);
                    } catch (Exception $e) {
                        $errorFileUploading = $e->getMessage();
                    }
                }
                if ($errorFileUploading != '') {
                    Mage::getSingleton('adminhtml/session')->addError(
                        Mage::helper('storelocator')->__($fileName." $errorFileUploading"));
                    $this->_redirect('*/*/');
                    return;
                }
            }
            
            $data['file_name'] = $fileName;
            $model->setData($data);
                
            try {
                $model->save();
                
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('storelocator')->__('The Store Point has been saved.')
                );
                    
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array(
                    'id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        
        $this->_redirect('*/*/');
    }
    
    /**
     * Deletes Point
     * 
     * @return
     */
    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                $model = Mage::getModel('storelocator/storelocator');
                $model->setId($id);

                $model->load($id);
                $model->delete();
                
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('storelocator')->__('The Store Point has been deleted.'));
                
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $id));
                return;
            }
        }
        
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('storelocator')->__('Unable to find a Store Point to delete.'));
        $this->_redirect('*/*/');
    }
    
    /**
     * Check is allowed access to action
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('system/config/storelocator');
    }
}

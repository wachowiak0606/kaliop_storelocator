<?php
/**
 * Magento
 *
 * @category    Kaliop
 * @package     Kaliop_StoreLocator
 * @copyright   Copyright (c) 2019 Kaliop Poland Sp. z o.o. (https://www.kaliop.pl/)
 * @license     ? http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author      Filip Wachowiak <filip0606@o2.pl>
 */
class Kaliop_StoreLocator_ListController extends Mage_Core_Controller_Front_Action
{
    /**
     * Returns Storelocator Default Helper
     * 
     * @return Kaliop_StoreLocator_Helper_Data
     */
    protected function _getHelper()
    {
        return Mage::helper('storelocator');
    }
    
    /**
     * Checks If Storelocator Shipping Method Is Enabled
     */
    public function preDispatch() 
    {
        parent::preDispatch();
        
        if (!$this->_getHelper()->isActive()) {
            $this->getResponse()->setRedirect(Mage::getBaseUrl());
        }
    }
    
    /**
     * Loads and Renders Layout
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
    
    /**
     * Loads And Renders Layout
     */
    public function detailsAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }    
}
